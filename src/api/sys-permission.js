import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/api/permission/list',
    method: 'post',
    params: data
  })
}

export function createRow(data) {
  return request({
    url: '/api/permission/add',
    method: 'post',
    data
  })
}

export function updateRow(data) {
  return request({
    url: '/api/permission/update',
    method: 'post',
    data
  })
}
export function deleteRow(data) {
  return request({
    url: '/api/permission/delete',
    method: 'post',
    params: data
  })
}
export function listTreePermission() {
  return request({
    url: '/api/permission/listTreePermission',
    method: 'post'
  })
}
